--- map on a list

map :: (a -> b) -> [a] -> [b]  
map _ [] = []  
map f (x:xs) = f x : map f xs  

-- fold on a tree

data Tree a = Leaf a
            | Fork (Tree a) a (Tree a)
     deriving Show


fold :: (a -> a -> a) -> Tree a -> a
fold f (Leaf n) = n
fold f (Fork l n r) = f n m
    where
        m = f (fold f l) (fold f r)