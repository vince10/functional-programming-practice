-- List Comprehensions and Higher-order functions

prop_A1  xs    = map (+1) xs                                       == [ x+1 | x <- xs :: [Int] ]
prop_A2  xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)        == [ x+y | x <- xs, y <- ys :: [Int] ]
prop_A3  xs    = map (+2) (filter (>3) xs)                         == [ x+2 | x <- xs :: [Int], x > 3 ]
prop_A4  xys   = map (\(x,_) -> x+3) xys                           == [ x+3 | (x,_) <- xys :: [(Int,Int)] ]
prop_A4' xys   = map ((+3) . fst) xys                              == [ x+3 | (x,_) <- xys :: [(Int,Int)] ]
prop_A5  xys   = map ((+4) . fst) (filter (\(x,y) -> x+y < 5) xys) == [ x+4 | (x,y) <- xys :: [(Int,Int)], x+y < 5 ]
prop_A6  mxs   = map (\(Just x) -> x+5) (filter isJust mxs)        == [ x+5 | Just x <- mxs :: [Maybe Int] ]


prop_B1  xs    = [ x+3 | x <- xs ]               == map (+3) (xs :: [Int])
prop_B2  xs    = [ x | x <- xs, x > 7 ]          == filter (>7) (xs :: [Int])
prop_B3  xs ys = [ (x,y) | x <- xs, y <- ys ]    == concat (map (\x -> map (\y -> (x,y)) (ys :: [Int])) (xs :: [Int]))
prop_B4  xys   = [ x+y | (x,y) <- xys, x+y > 3 ] == filter (>3) (map (\(x,y) -> x+y) (xys :: [(Int,Int)]