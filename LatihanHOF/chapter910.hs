

-- Define the length function using map and sum.
len :: [a] -> Int
len xs = sum (map (\x -> 1) xs)
 
-- What does map (+1) (map (+1) xs)do? Can you conclude anything in general about properties of map f (map g xs), where f and g are arbitrary functions?
-- Answer : it will increment all of the members of xs by one, and the put the incremented members of xs in a new list. then it will increment all the members of the new list by one, and the 'double incremented' members will be stored in another new list. basically map f (map g xs ) is a notation of doubble mapping
 
--Give the type of, and define the functioniterso that•iter n f x = f (f (... (f x)))wherefoccursntimes on theright-hand side of the equation. For instance, we should haveiter 3 f x = f (f (f x))anditer 0 f xshould returnx
 
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)
 
-- (*) How would you define the sum of the squares of the natural numbers 1 to n using map and foldr?

sumSquaress :: Integer -> Integer
sumSquaress n = foldr (+) 0 $ map (\x -> x*x) [1..n]

-- mystery xs function behaviour



--FLip function

flipss :: (a -> b -> c) -> (b -> a -> c)
flipss f = \x y -> f y x

--(*) If id is the polymorphic identity function, defined by id x = x, explain the
--behavior of the expressions
-- • (id . f) (f . id) (id f)
--f f is of type Int -> Bool, at what instance of its most general type a ->
--a is id used in each case?


--Answer :   
{-
--(id . f)  is the same as f, because the result of f is fed to the
--identity function id, and thus not changed
--(id :: Bool -> Bool)

--(f . id)  is the same as f, because the argument of f is fed to the
--identity function id, and thus not changed
--(id :: Int -> Int)

--id f      is the same as f, because applying id to something does not
--change it
--(id :: (Int -> Bool) -> (Int -> Bool))
--}


-- Define a function composeList which composes a list of functions into a single
--function. You should give the type of composeList, and explain why the function
--has this type. What is the effect of your function on an empty list of functions?

--Answer :
composeList :: [a -> a] -> (a -> a)
composeList = foldr (.) id
 
