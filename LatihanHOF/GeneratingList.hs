-- Write a generator

listOfLength :: Int -> Gen a -> Gen [a]
listOfLength n gen = sequence [ gen | i <- [1..n] ]

--Now use listOf to write a generator that generates pairs of lists of the same, random,
--length
pairsOfEqualLengthLists :: Gen a -> Gen ([a],[a])
pairsOfEqualLengthLists gen =
  do n <- choose (0,100)
     xs <- listOfLength (abs n) gen
     ys <- listOfLength (abs n) gen
     return (xs,ys)

-- zip and unzip

data TwoSameLengthLists a = SameLength [a] [a]
 deriving (Show)

instance Arbitrary a => Arbitrary (TwoSameLengthLists a) where
  arbitrary =
    do (xs,ys) <- pairsOfEqualLengthLists arbitrary
       return (SameLength xs ys)

prop_UnzipZip1 :: TwoSameLengthLists Int -> Bool
prop_UnzipZip1 (SameLength xs ys) =
  unzip (zip xs ys) == (xs,ys)




